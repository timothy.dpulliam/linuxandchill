<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom"><title>Linux And Chill - System Administration</title><link href="http://linuxandchill.xyz.s3-website.us-east-1.amazonaws.com/" rel="alternate"></link><link href="http://linuxandchill.xyz.s3-website.us-east-1.amazonaws.com/feeds/system-administration.atom.xml" rel="self"></link><id>http://linuxandchill.xyz.s3-website.us-east-1.amazonaws.com/</id><updated>2019-05-10T10:13:00-04:00</updated><entry><title>A Docker Primer</title><link href="http://linuxandchill.xyz.s3-website.us-east-1.amazonaws.com/a-docker-primer.html" rel="alternate"></link><published>2019-05-10T10:13:00-04:00</published><updated>2019-05-10T10:13:00-04:00</updated><author><name>Timothy Pulliam</name></author><id>tag:linuxandchill.xyz.s3-website.us-east-1.amazonaws.com,2019-05-10:/a-docker-primer.html</id><summary type="html">&lt;p&gt;A short introduction to Docker&lt;/p&gt;</summary><content type="html">&lt;h2&gt;Terminology&lt;/h2&gt;
&lt;h5&gt;Image vs. Container&lt;/h5&gt;
&lt;p&gt;A Docker image can be thought of as the blueprint for an OS that will be used to construct a container. An image is essentially how the the container will be configured, while a container is the actual running OS itself.&lt;/p&gt;
&lt;p&gt;A single image can generate many containers.&lt;/p&gt;
&lt;h2&gt;Installation&lt;/h2&gt;
&lt;p&gt;The following is only for installing the docker community edition. For other
options installing Docker, see the &lt;a href="https://docs.docker.com/install/"&gt;Docker Docs&lt;/a&gt;.&lt;/p&gt;
&lt;p&gt;Ubuntu&lt;/p&gt;
&lt;div class="highlight"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;apt-get update
apt-get install docker-ce
&lt;/pre&gt;&lt;/div&gt;


&lt;p&gt;Redhat&lt;/p&gt;
&lt;div class="highlight"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;yum install docker-ce
&lt;/pre&gt;&lt;/div&gt;


&lt;p&gt;Start the docker daemon&lt;/p&gt;
&lt;div class="highlight"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;systemctl start docker

systemctl enable docker
&lt;/pre&gt;&lt;/div&gt;


&lt;h2&gt;Pulling Images From Docker Hub&lt;/h2&gt;
&lt;p&gt;Community Images are uploaded to the &lt;a href="https://hub.docker.com/"&gt;Docker Hub&lt;/a&gt; where
anyone can pull them. The concept is very similar to source code and Github.&lt;/p&gt;
&lt;p&gt;You can search for a particular image, say kali linux.&lt;/p&gt;
&lt;div class="highlight"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;tpulliam@lappy:~$ docker search kali
NAME                           DESCRIPTION                                     STARS               OFFICIAL            AUTOMATED
kalilinux/kali-linux-docker    Kali Linux Rolling Distribution Base Image      555                                     [OK]
linuxkonsult/kali-metasploit   Kali base image with metasploit                 68

...
&lt;/pre&gt;&lt;/div&gt;


&lt;p&gt;Pull the one with the most stars, or the official one. Whichever.&lt;/p&gt;
&lt;div class="highlight"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;tpulliam@lappy:~$ docker pull kalilinux/kali-linux-docker

Using default tag: latest
latest: Pulling from kalilinux/kali-linux-docker
014a6d74f96c: Pull complete
9febb14563a0: Pull complete
c38f04972c6b: Pull complete
9d39d049d5d0: Pull complete
4e80058918bf: Pull complete
ccd85f0810ad: Pull complete
6ab25bddb799: Pull complete
789ba1ebcb41: Pull complete
524584107b55: Pull complete
40a1af1b9680: Pull complete
fb6edc730b9f: Pull complete
270f87c17cf3: Pull complete
8159e4fffac9: Pull complete
01d3dc4e9fe9: Pull complete
a386beee6e20: Pull complete
46fa80bd0f33: Pull complete
2d3ff71a091d: Pull complete
6f41a333af7c: Pull complete
Digest: sha256:7f1253cba663662d78c121369e23d6c0b58b7b95de268886ecfdb73b003a3bd8
Status: Downloaded newer image for kalilinux/kali-linux-docker:latest
&lt;/pre&gt;&lt;/div&gt;


&lt;p&gt;We can see the kali linux image has been added to the list of images.&lt;/p&gt;
&lt;div class="highlight"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;tpulliam@lappy:~$ docker images
REPOSITORY                    TAG                 IMAGE ID            CREATED             SIZE
kalilinux/kali-linux-docker   latest              f26f3ae90aee        2 months ago        1.57GB
&lt;/pre&gt;&lt;/div&gt;


&lt;h2&gt;Running An Instance/Container&lt;/h2&gt;
&lt;p&gt;There are two ways to run a container. You can run a container interactively,
where you have access to the shell&lt;/p&gt;
&lt;div class="highlight"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;tpulliam@lappy:~$ docker run -t -i kalilinux/kali-linux-docker /bin/bash
root@f157bf90bca9:/#
&lt;/pre&gt;&lt;/div&gt;


&lt;p&gt;Or you can instruct the container to run a single command and then terminate&lt;/p&gt;
&lt;div class="highlight"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;tpulliam@lappy:~$ docker run kalilinux/kali-linux-docker echo &amp;quot;hello docker&amp;quot;
hello docker
&lt;/pre&gt;&lt;/div&gt;


&lt;p&gt;To view all running and stopped containers use the &lt;code&gt;docker ps -a&lt;/code&gt; command. If
you only want to see running containers (and not stopped/terminated ones) ommit
the &lt;code&gt;-a&lt;/code&gt; flag.&lt;/p&gt;
&lt;div class="highlight"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;tpulliam@lappy:~$ docker ps -a
CONTAINER ID        IMAGE                         COMMAND             CREATED              STATUS              PORTS               NAMES
f157bf90bca9        kalilinux/kali-linux-docker   &amp;quot;/bin/bash&amp;quot;         About a minute ago   Up About a minute                       objective_golick
37be294f38ab        kalilinux/kali-linux-docker   &amp;quot;echo &amp;#39;hello docker&amp;#39;&amp;quot;   39 seconds ago      Exited (0) 37 seconds ago                       elegant_mccarthy
&lt;/pre&gt;&lt;/div&gt;


&lt;p&gt;If we need to reattach to the container, we can do so by specifying the container
name or id.&lt;/p&gt;
&lt;div class="highlight"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;tpulliam@lappy:~$ docker attach objective_golick
root@f157bf90bca9:/#
&lt;/pre&gt;&lt;/div&gt;


&lt;p&gt;To stop and remove a running instance run the below commands (you can interchange container name and id). You can verify the containers have been removed by running
&lt;code&gt;docker ps -a&lt;/code&gt;.&lt;/p&gt;
&lt;div class="highlight"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;tpulliam@lappy:~$ docker stop objective_golick
objective_golick
tpulliam@lappy:~$ docker rm f157bf90bca9
f157bf90bca9
&lt;/pre&gt;&lt;/div&gt;</content><category term="Docker"></category><category term="Devops"></category></entry><entry><title>Custom Firewalld Services</title><link href="http://linuxandchill.xyz.s3-website.us-east-1.amazonaws.com/custom-firewalld-services.html" rel="alternate"></link><published>2019-05-08T19:05:00-04:00</published><updated>2019-05-08T19:05:00-04:00</updated><author><name>Timothy Pulliam</name></author><id>tag:linuxandchill.xyz.s3-website.us-east-1.amazonaws.com,2019-05-08:/custom-firewalld-services.html</id><summary type="html">&lt;h2&gt;Overview&lt;/h2&gt;
&lt;p&gt;You can define custom services using firewall-cmd. Services are groups of firewall rules. For example, on a freeipa server, you would need to open the following ports.&lt;/p&gt;
&lt;p&gt;TCP Ports:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;80, 443: HTTP/HTTPS&lt;/li&gt;
&lt;li&gt;389, 636: LDAP/LDAPS&lt;/li&gt;
&lt;li&gt;88, 464: Kerberos&lt;/li&gt;
&lt;li&gt;53: DNS&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;UDP Ports:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;88, 464: Kerberos&lt;/li&gt;
&lt;li&gt;53: DNS …&lt;/li&gt;&lt;/ul&gt;</summary><content type="html">&lt;h2&gt;Overview&lt;/h2&gt;
&lt;p&gt;You can define custom services using firewall-cmd. Services are groups of firewall rules. For example, on a freeipa server, you would need to open the following ports.&lt;/p&gt;
&lt;p&gt;TCP Ports:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;80, 443: HTTP/HTTPS&lt;/li&gt;
&lt;li&gt;389, 636: LDAP/LDAPS&lt;/li&gt;
&lt;li&gt;88, 464: Kerberos&lt;/li&gt;
&lt;li&gt;53: DNS&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;UDP Ports:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;88, 464: Kerberos&lt;/li&gt;
&lt;li&gt;53: DNS&lt;/li&gt;
&lt;li&gt;123: NTP&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;You can group these all into a single service (named freeipa) and then simply start the one service, rather than start each individual rule. This allows you to logically group firewall rules so they are started and stopped together if necessary. It also avoids the inevitable confusion when you list out the ports on your firewall and see that port 88/UDP is open you may wonder why.&lt;/p&gt;
&lt;h2&gt;Creating New services&lt;/h2&gt;
&lt;p&gt;The following command creates a new file named &lt;code&gt;/etc/firewalld/services/freeipa.xml&lt;/code&gt; as well as a new firewalld service named &lt;code&gt;freeipa&lt;/code&gt;.&lt;/p&gt;
&lt;div class="highlight"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;$ firewall-cmd --permanent --new-service=freeipa
&lt;/pre&gt;&lt;/div&gt;


&lt;p&gt;You can add a more descriptive name as well as a short name for the service, plus the ports you wish to be associated with this service.&lt;/p&gt;
&lt;div class="highlight"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;$ firewall-cmd --permanent --service=freeipa --set-description=&amp;quot;Firewall rules for a FreeIPA Server&amp;quot;
$ firewall-cmd --permanent --service=freeipa --set-short=freeipa
$ firewall-cmd --permanent --service=freeipa --add-port={80/tcp,443/tcp,389/tcp,636/tcp,88/tcp,464/tcp,53/tcp,88/udp,464/udp,53/udp,123/udp}
&lt;/pre&gt;&lt;/div&gt;


&lt;p&gt;Now if you examine &lt;code&gt;/etc/firewalld/services/freeipa.xml&lt;/code&gt; you will see it has been configured&lt;/p&gt;
&lt;div class="highlight"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;$ cat /etc/firewalld/services/freeipa.xml
&amp;lt;?xml version=&amp;quot;1.0&amp;quot; encoding=&amp;quot;utf-8&amp;quot;?&amp;gt;
&amp;lt;service&amp;gt;
  &amp;lt;short&amp;gt;freeipa&amp;lt;/short&amp;gt;
  &amp;lt;description&amp;gt;Firewall rules for a FreeIPA Server&amp;lt;/description&amp;gt;
  &amp;lt;port protocol=&amp;quot;tcp&amp;quot; port=&amp;quot;80&amp;quot;/&amp;gt;
  &amp;lt;port protocol=&amp;quot;tcp&amp;quot; port=&amp;quot;443&amp;quot;/&amp;gt;
  &amp;lt;port protocol=&amp;quot;tcp&amp;quot; port=&amp;quot;389&amp;quot;/&amp;gt;
  &amp;lt;port protocol=&amp;quot;tcp&amp;quot; port=&amp;quot;636&amp;quot;/&amp;gt;
  &amp;lt;port protocol=&amp;quot;tcp&amp;quot; port=&amp;quot;88&amp;quot;/&amp;gt;
  &amp;lt;port protocol=&amp;quot;tcp&amp;quot; port=&amp;quot;464&amp;quot;/&amp;gt;
  &amp;lt;port protocol=&amp;quot;tcp&amp;quot; port=&amp;quot;53&amp;quot;/&amp;gt;
  &amp;lt;port protocol=&amp;quot;udp&amp;quot; port=&amp;quot;88&amp;quot;/&amp;gt;
  &amp;lt;port protocol=&amp;quot;udp&amp;quot; port=&amp;quot;464&amp;quot;/&amp;gt;
  &amp;lt;port protocol=&amp;quot;udp&amp;quot; port=&amp;quot;53&amp;quot;/&amp;gt;
  &amp;lt;port protocol=&amp;quot;udp&amp;quot; port=&amp;quot;123&amp;quot;/&amp;gt;
&amp;lt;/service&amp;gt;
&lt;/pre&gt;&lt;/div&gt;


&lt;p&gt;Now, you can add this service just like any other service&lt;/p&gt;
&lt;div class="highlight"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;# firewall-cmd --permanent --add-service freeipa
# firewall-cmd --reload
# firewall-cmd --list-services
ssh dhcpv6-client dns http freeipa
&lt;/pre&gt;&lt;/div&gt;


&lt;p&gt;If you wish to permanently delete the service. You need to reload firewalld in order for effects to be recognized.&lt;/p&gt;
&lt;div class="highlight"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;# firewall-cmd --remove-service=freeipa
# firewall-cmd --permanent --delete-service=freeipa
success
# firewall-cmd --reload
&lt;/pre&gt;&lt;/div&gt;


&lt;p&gt;The *.xml config files are now deleted&lt;/p&gt;
&lt;div class="highlight"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;# pwd
/etc/firewalld/services
# ls
freeipa.xml.old  test.xml.old
&lt;/pre&gt;&lt;/div&gt;


&lt;h4&gt;Resources&lt;/h4&gt;
&lt;p&gt;&lt;a href="http://www.firewalld.org/documentation/howto/add-a-service.html"&gt;http://www.firewalld.org/documentation/howto/add-a-service.html&lt;/a&gt;&lt;/p&gt;</content><category term="Networking"></category><category term="Firewall"></category></entry><entry><title>Automating RHEL 7 / CentOS 7 Updates With yum-cron</title><link href="http://linuxandchill.xyz.s3-website.us-east-1.amazonaws.com/automating-rhel-7-centos-7-updates-with-yum-cron.html" rel="alternate"></link><published>2018-04-21T01:39:00-04:00</published><updated>2018-04-21T01:39:00-04:00</updated><author><name>Timothy Pulliam</name></author><id>tag:linuxandchill.xyz.s3-website.us-east-1.amazonaws.com,2018-04-21:/automating-rhel-7-centos-7-updates-with-yum-cron.html</id><summary type="html">&lt;h2&gt;Overview&lt;/h2&gt;
&lt;p&gt;I’m sure by now every one knows about yum-cron. It automatically updates your systems packages, which can be handy if you have to manage lots of systems and you want to make sure the latest security patches have been installed. It’s also pretty straight forward to use …&lt;/p&gt;</summary><content type="html">&lt;h2&gt;Overview&lt;/h2&gt;
&lt;p&gt;I’m sure by now every one knows about yum-cron. It automatically updates your systems packages, which can be handy if you have to manage lots of systems and you want to make sure the latest security patches have been installed. It’s also pretty straight forward to use. Just be careful not to automatically update systems such as your production web server without testing, as some updates can actually break things. You’ve been warned.&lt;/p&gt;
&lt;h3&gt;Installation&lt;/h3&gt;
&lt;p&gt;Install the package the usual way&lt;/p&gt;
&lt;div class="highlight"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;# yum install yum-cron
&lt;/pre&gt;&lt;/div&gt;


&lt;p&gt;Make sure the service is running&lt;/p&gt;
&lt;div class="highlight"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;# systemctl start yum-cron
# systemctl enable yum-cron
&lt;/pre&gt;&lt;/div&gt;


&lt;h3&gt;Configuration&lt;/h3&gt;
&lt;p&gt;There are two configuration files for yum cron. There are also two crontab entries for yum-cron&lt;/p&gt;
&lt;div class="highlight"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;# rpm -qc yum-cron
/etc/yum/yum-cron-hourly.conf
/etc/yum/yum-cron.conf

# rpm -ql yum-cron
/etc/cron.daily/0yum-daily.cron
/etc/cron.hourly/0yum-hourly.cron
…
…
&lt;/pre&gt;&lt;/div&gt;


&lt;p&gt;The &lt;em&gt;/etc/cron.daily/0yum-daily.cron&lt;/em&gt; job reads from the &lt;em&gt;/etc/yum/yum-cron.conf&lt;/em&gt; configuration file while the &lt;em&gt;/etc/cron.hourly/0yum-hourly.cron&lt;/em&gt; file reads from the &lt;em&gt;/etc/yum/yum-cron-hourly.conf&lt;/em&gt; configuration file. The two configuration files are meant to optimize yum-cron depending on whether it runs hourly or daily. If you want to disable hourly checks, you can edit the &lt;em&gt;/etc/cron.hourly/0yum-hourly.cron&lt;/em&gt; and comment out the exec line like so&lt;/p&gt;
&lt;div class="highlight"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;# Action!
# exec /usr/sbin/yum-cron /etc/yum/yum-cron-hourly.conf
&lt;/pre&gt;&lt;/div&gt;


&lt;p&gt;Configuration of /etc/yum/yum-cron.conf is straight forward. The only things you might need to set are&lt;/p&gt;
&lt;div class="highlight"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;[commands]
apply_updates = yes
&lt;/pre&gt;&lt;/div&gt;


&lt;p&gt;Otherwise, updates will not actually be applied ;b&lt;/p&gt;
&lt;p&gt;You may also want to disable upgrading the kernel itself as this can effect your system in unexpected ways.&lt;/p&gt;
&lt;div class="highlight"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;[base]
exclude = kernel*
&lt;/pre&gt;&lt;/div&gt;


&lt;p&gt;yum-cron can also alert you when your system is updated. This can be set via the following options&lt;/p&gt;
&lt;div class="highlight"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;[emitters]
system_name = your_hostname
emit_via = email

[email]
email_to = your_email@domain.com
&lt;/pre&gt;&lt;/div&gt;


&lt;p&gt;You will also need to have an MTA like postfix running to be able to send emails.&lt;/p&gt;
&lt;div class="highlight"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;# yum install postfix
# systemctl start postfix
# systemctl enable postfix
&lt;/pre&gt;&lt;/div&gt;</content><category term="Automation"></category></entry><entry><title>Avoiding GID "conflicts" when Creating Users/Groups In Linux</title><link href="http://linuxandchill.xyz.s3-website.us-east-1.amazonaws.com/avoiding-gid-conflicts-when-creating-usersgroups-in-linux.html" rel="alternate"></link><published>2018-02-11T10:14:00-05:00</published><updated>2018-02-11T10:14:00-05:00</updated><author><name>Timothy Pulliam</name></author><id>tag:linuxandchill.xyz.s3-website.us-east-1.amazonaws.com,2018-02-11:/avoiding-gid-conflicts-when-creating-usersgroups-in-linux.html</id><summary type="html">&lt;p&gt;Often times when you first install your OS the first thing you want to do is add some users and groups. So let's go ahead and do that.&lt;/p&gt;
&lt;div class="highlight"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;  [root@localhost ~]# useradd daisy
  [root@localhost ~]# groupadd cats
  [root@localhost ~]# useradd sophie
&lt;/pre&gt;&lt;/div&gt;


&lt;p&gt;But when we go to check on our newly created …&lt;/p&gt;</summary><content type="html">&lt;p&gt;Often times when you first install your OS the first thing you want to do is add some users and groups. So let's go ahead and do that.&lt;/p&gt;
&lt;div class="highlight"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;  [root@localhost ~]# useradd daisy
  [root@localhost ~]# groupadd cats
  [root@localhost ~]# useradd sophie
&lt;/pre&gt;&lt;/div&gt;


&lt;p&gt;But when we go to check on our newly created users we see that the "sophie" user has a UID of 1001, but a GID of 1002. This could complicate things unnecessarily. So how do we prevent this?&lt;/p&gt;
&lt;div class="highlight"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;  [root@localhost ~]# tail -2 /etc/passwd
  daisy:x:1000:1000::/home/daisy:/bin/bash
  sophie:x:1001:1002::/home/sophie:/bin/bash
&lt;/pre&gt;&lt;/div&gt;


&lt;p&gt;We can see that the reason sophie was forced to take a GID of 1002, is because we had given the newly created "cats" group a GID of 1001.&lt;/p&gt;
&lt;div class="highlight"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;[root@localhost ~]# tail -3 /etc/group
daisy:x:1000:
cats:x:1001:
sophie:x:1002:
&lt;/pre&gt;&lt;/div&gt;


&lt;p&gt;An easy way to fix this is to designate a range of GIDs when creating groups (normally around 5000+) so they don't interfere with users in the 1000+ range. For example, what we could have done was the following.&lt;/p&gt;
&lt;div class="highlight"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;  [root@localhost ~]# useradd daisy
  [root@localhost ~]# groupadd -g 5000 cats
  [root@localhost ~]# useradd sophie
  [root@localhost ~]# tail -2 /etc/passwd
  daisy:x:1000:1000::/home/daisy:/bin/bash
  sophie:x:1001:1001::/home/sophie:/bin/bash
&lt;/pre&gt;&lt;/div&gt;


&lt;p&gt;Now we can see that users GIDs will never interfere with GIDs of newly created groups. Furthermore we only need to pass the groupadd -g flag the first time. After that, groupadd will know to give groups a GID that comes after the one highest in the /etc/group file.&lt;/p&gt;
&lt;div class="highlight"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;[root@localhost ~]# tail -5 /etc/group
stapdev:x:158:
tcpdump:x:72:
daisy:x:1000:
cats:x:5000:
sophie:x:1001:
[root@localhost ~]# groupadd people
[root@localhost ~]# tail -5 /etc/group
tcpdump:x:72:
daisy:x:1000:
cats:x:5000:
sophie:x:1001:
people:x:5001:
&lt;/pre&gt;&lt;/div&gt;</content><category term="Users and Groups"></category></entry></feed>