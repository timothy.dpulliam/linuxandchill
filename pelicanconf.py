#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# Site Settings
SITENAME = 'Linux And Chill'
SITEURL = 'http://linuxandchill.xyz.s3-website.us-east-1.amazonaws.com'
TIMEZONE = 'America/New_York'
DEFAULT_LANG = 'en'
DEFAULT_METADATA = {
        'status': 'draft',
}

# Content Settings
AUTHOR = u'Timothy Pulliam'
OUTPUT_PATH = 'public'
PATH = 'content'
# USE_FOLDER_AS_CATEGORY = True
STATIC_PATHS = ['static/images', 'static/extra', 'static/js']
EXTRA_PATH_METADATA = {
    'extra/CNAME': {'path': 'CNAME'},
    'static/extra/favicon.ico': {'path': 'favicon.ico'},
}
THEME = "themes/notmyidea"

# Plugin Settings
PLUGIN_PATHS = ['plugins']
PLUGINS = ['render_math', 'photos']
# Photos Plugin Settings
PHOTO_LIBRARY = "content/static/images"
PHOTO_ARTICLE = (760, 506, 80)
# Comment System
PELICAN_COMMENT_SYSTEM = True
PELICAN_COMMENT_SYSTEM_DIR = 'content/comments'
PELICAN_COMMENT_SYSTEM_IDENTICON_DATA = ('author',)

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),)

# Social widget
SOCIAL = (('LinkedIn', 'https://linkedin.com/in/timothy-pulliam'),
          ('Github', 'https://github.com/Timothy-Pulliam'),
           ('YouTube', 'https://www.youtube.com/channel/UC0G4YCuFJukeTicFBO6u4-A?view_as=subscriber'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

# https://python-markdown.github.io/extensions/code_hilite/
MARKDOWN = {'extension_configs': {'markdown.extensions.codehilite': {'css_class': 'highlight', 'use_pygments': True, 'guess_lang': False,},
                                    'markdown.extensions.extra': {},
                                    'markdown.extensions.meta': {}},
                                    'output_format': 'html5'}
