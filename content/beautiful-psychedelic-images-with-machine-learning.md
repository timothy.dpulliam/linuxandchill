Title: Beautiful Psychedelic Images with Deep Dream Generator
Date: 2019-05-19 10:20
Category: Machine Learning
Tags: machine learning, art
Slug: beautiful-psychedelic-images-with-deep-dream-generator
Authors: Timothy Pulliam
Summary: Beautiful art generated with the Deep Dream Generator and Machine Learning
Status: published




The [Deep Dream Generator](https://deepdreamgenerator.com/) uses Machine Learning to combine multiple images. Essentially, you give the ML algorithm two images, and it will "look" for features of one image in another image. The more iterations of the algorithm that take place, the more prominent the features become.

For example, here is a picture of my wife and I enjoying a lovely sunset.

![My Beautiful Wife and I]({photo}beautiful-psychedelic-images-with-machine-learning/Nicole_Tim_Sunset.jpg)

Combined with another picture from Deep Dream Generator's catalog yields the following image.

![My Beautiful Wife and I]({photo}beautiful-psychedelic-images-with-machine-learning/nicole_dream1.png)

Running the image through another iteration brings out more of the features of the second image

![My Beautiful Wife and I]({photo}beautiful-psychedelic-images-with-machine-learning/nicole_dream2.png)

Here is the same photo of us combined with Vincent van Gogh's Starry Night.

![Starry Night]({photo}beautiful-psychedelic-images-with-machine-learning/nicole_tim_starry_night.jpg)

And because the internet loves cats, I combined a picture of one of my beautiful cats (her name is Daisy).

![Daisy the Cat]({photo}beautiful-psychedelic-images-with-machine-learning/Daisy.jpg)

With an image from a concert my wife and I attended some time ago. Very Mesmerizing.

![Mesmerizing Concert Hall]({photo}beautiful-psychedelic-images-with-machine-learning/Zelda_Concert_Hall.jpg)

Which yielded the following image. You can see the chandelier in some places.

![Psychedelic Daisy]({photo}beautiful-psychedelic-images-with-machine-learning/daisy_dream1.jpg)

So what will you create? Maybe your next album cover? Or a nice conversation piece to hang in your living room? Your imagination is the limit!
