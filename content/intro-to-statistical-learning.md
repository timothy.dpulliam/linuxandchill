Title: Reducible vs Irreducible Error
Date: 2019-05-14 10:20
Category: Machine Learning
Tags: statistics, machine learning
Slug: reducible-vs-irreducible-error
Authors: Timothy Pulliam
Summary: An overview of Reducible and IrreducibleError applied to Statistical Learning
Status: published

### Statistical Learning Fundamentals
Consider the following data, which shows relationships between median values of Boston homes in 1978 against three predictors.

![MED vs CRIM]({static}static/images/reducible-vs-irreducible-error/crim.png)

![MED vs CRIM]({static}static/images/reducible-vs-irreducible-error/nox.png)

![MED vs CRIM]({static}static/images/reducible-vs-irreducible-error/ptratio.png)

with dependent variable

* y = MEDV - Median value of owner-occupied homes in $1000's

and independent variables

* $x_1$ = CRIM - per capita crime rate by town
* $x_2$ = NOX - nitric oxides concentration (parts per 10 million)
* $x_3$ = PTRATIO - pupil-teacher ratio by town
* $X = (x_1,x_2,x_3)$

We also recognize that the data points have some inherit uncertainty in their
measurements. This uncertainty or error ($\epsilon$), can result from many factors. The
measurements may simply be taken using inaccurate methods or tools. Or it could be that there are other independent variables which we have not
taken into account for our data.

Thus, we have the following relationship

$$Y(x_1, x_2, x_3) = f(x_1, x_2, x_3) + \epsilon$$

$$Y(X) = f(X) + \epsilon$$


Where $f$ is some unknown function that theoretically captures the data if there were
no error ($\epsilon$) in our measurements.

![Underlying Function f(x)]({static}static/images/reducible-vs-irreducible-error/irreducable_error.png)

In theory, $\epsilon$ is a random error term and has mean zero.

![Irreducible Error]({static}static/images/reducible-vs-irreducible-error/error-distribution.png)

* $Y(X)$ = the data that was collected
* $f(X)$ = a theoretical function describing $Y(x)$
* $\epsilon$ = A random error term. It is sometimes called "noise". The errors are
generated at measurement time. Mathematically, it is the difference between our theoretical function and the collected data due to inaccuracies during the data collection process. It is independent of $X$ and is assumed to have mean 0.

### Reducible vs Irreducible Error
##### Deriving Expected Value
In many cases we are given some X, but we need to know the corresponding $Y(X)$.
For this reason, we are interested in obtaining $f(x)$ (if it is possible) so that we can make accurate predictions for any given input variable. However, in most cases, we
don't know $f(x)$ and we will not be able to obtain it, since it is obfuscated
behind the random errors. Instead, we guess the function using

$$\hat Y(X) = \hat f(X)$$

where $\hat f(x)$ is our estimate for the theoretical underlying function $f(x)$, and $\hat Y(X)$ is the resulting prediction of $Y(X)$.

<!-- Going Back to our Bostong housing example, we wish to accurately predict the
median value of a home, given some information about the town the home resides in.
We use the following equations

$$Y(X) = f(X) + \epsilon$$
$$\hat Y(X) = \hat f(X)$$ -->

How accurately $\hat Y(X)$ predicts $Y(X)$ depends upon *reducible error* and the
*irreducible error*. The reducible error is error we have introduced by selecting
$\hat f(X)$ which does not accurately predict $Y(X)$. It is called reducible
because we can reduce it by finding another function that more closely
approximates $f(X)$. The irreducible error is the error which resulted at the
time the data was recorded and are reflected in the random $\epsilon$ term. The
irreducible error term is typically unknown in practice (if we knew the source
of it we might be able to reduce it and it would no longer be irreducible!).
Thus, we are only interested in the reducible error, since it is the only one within
our control.

We can calculate how closely our prediction approximates the data by
calculating the expected value of the square of the difference between them.
That is

$$E \left[ \left( Y(X) - \hat Y(X) \right)^2 \right] = E\left [\left( f(X) + \epsilon - \hat f(X)\right)^2 \right]$$
$$= E \left[ f^2 +f\epsilon - f \hat f + \epsilon f + \epsilon^2 - \hat f \epsilon - \hat f f - \hat f \epsilon + \hat f ^2 \right]$$
$$ = E \left[ (f - \hat f)^2 + \epsilon^2 - 2\epsilon\hat f + 2\epsilon f)\right]$$

If we recall the the Expected Value is a [linear transformation](https://en.wikipedia.org/wiki/Expected_value#Linearity) akin to an integral
or summation, we can reduce as follows

$$=E[(f-\hat f)^2] + E \left [\epsilon^2 - 2\epsilon\hat f + 2\epsilon f) \right]$$

If we make the following assumptions

* $X$ = some constant (then $\hat f$ and $f$ are both constants as well)
* $\epsilon$ is a random variable
* $\epsilon$ does not depend upon $X$ in any way
* $\epsilon$ has expected value (mean) 0

$$ E \left [\epsilon^2 - 2\epsilon\hat f + 2\epsilon f) \right] = E[\epsilon^2]
- 2 \hat f E[\epsilon] + 2fE[\epsilon]$$

Noting that the variance of $\epsilon = E[\epsilon^2] - E[\epsilon]^2 = E[\epsilon^2]$
We finally have

$$E \left[ \left( Y(X) - \hat Y(X) \right)^2 \right] = \underset{Reducible\ Error}{E[(f-\hat f)^2]} + \underset{Irreducible Error}{Var(\epsilon)}$$

The reducible error term. The closely our estimate $\hat f$
approximates the true value of $f$, the more accurate our predictions will be

The irreducible error. There is no way we can modify our model
to minimize this error. This effectively puts an upper bounds on how accurate a
Machine Learning model can be.
